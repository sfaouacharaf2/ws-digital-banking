package com.project.bankingManage.repository;

import com.project.bankingManage.model.Customer;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerRepository extends CrudRepository<Customer, Long> {

    @Query("select c from Customer c where c.firstName like :kw")
    List<Customer> searchCustomer(@Param("kw") String keyword);
}
