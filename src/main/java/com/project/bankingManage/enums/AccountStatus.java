package com.project.bankingManage.enums;

public enum AccountStatus {

    CREATED,

    ACTIVATED,

    SUSPENDED
}
