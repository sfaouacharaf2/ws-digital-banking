package com.project.bankingManage.enums;

public enum OperationType {

    DEBIT,

    CREDIT
}
