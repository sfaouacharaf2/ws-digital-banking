package com.project.bankingManage.service;

import com.project.bankingManage.dto.CustomerDTO;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * service User.
 */
public interface CustomerService {

    CustomerDTO saveCustomer(CustomerDTO customerDTO);

    List<CustomerDTO> listCustumers();

    CustomerDTO getCustomer(Long customerID);

    CustomerDTO updateCustomer(CustomerDTO customerDTO);

    void deleteCustomer(Long customerId);

    List<CustomerDTO> searchCustomers(String keyword);
}
