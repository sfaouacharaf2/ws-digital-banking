package com.project.bankingManage.service;

import com.project.bankingManage.dto.*;
import com.project.bankingManage.exceptions.BalanceNotSufficientException;
import com.project.bankingManage.exceptions.BankAccountNotFoundException;
import com.project.bankingManage.exceptions.CustomerNotFoundException;

import java.util.List;

public interface BankAccountService {

    CurrentBankAccountDTO saveCurrentBankAccount(double initaleBalance, double overDraft, Long customerId) throws CustomerNotFoundException;

    SavingBankAccountDTO saveSavingBankAccount(double initaleBalance, double interestRate, Long customerId) throws CustomerNotFoundException;

    BankAccountDTO getBankAccount(String accountId) throws BankAccountNotFoundException;

    void debit(String accountId, double amount, String description) throws BankAccountNotFoundException, BalanceNotSufficientException;

    void credit(String accountId, double amount, String description) throws BankAccountNotFoundException, BalanceNotSufficientException;

    void transfer(String accountIdSource, String accountIdDestination, double amount) throws BankAccountNotFoundException, BalanceNotSufficientException;

    List<BankAccountDTO> bankAccountList();

    List<AccountOperationDTO> accountHistory(String accountId);

    AccountHistoryDto getAccountHistory(String accountId, int page, int size) throws BankAccountNotFoundException;
}
