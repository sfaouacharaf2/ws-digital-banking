package com.project.bankingManage;

import com.project.bankingManage.dto.BankAccountDTO;
import com.project.bankingManage.dto.CurrentBankAccountDTO;
import com.project.bankingManage.dto.CustomerDTO;
import com.project.bankingManage.dto.SavingBankAccountDTO;
import com.project.bankingManage.exceptions.CustomerNotFoundException;
import com.project.bankingManage.service.BankAccountService;
import com.project.bankingManage.service.CustomerService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.List;
import java.util.stream.Stream;

@SpringBootApplication
public class BankingManageBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(BankingManageBackendApplication.class, args);
    }

    @Bean
    CommandLineRunner commandLineRunner(BankAccountService bankAccountService, CustomerService customerService) {
        return args -> {
            Stream.of("philipe", "Loven", "nora").forEach(name -> {
                CustomerDTO customerDTO = new CustomerDTO();
                customerDTO.setFirstName(name);
                customerDTO.setLastName(name);
                customerDTO.setEmail(name + "@gmail.com");
                customerService.saveCustomer(customerDTO);
            });
            customerService.listCustumers().forEach(customer -> {
                try {
                    bankAccountService.saveCurrentBankAccount(Math.random() * 9000, 9000, customer.getId());
                    bankAccountService.saveSavingBankAccount(Math.random() * 120000, 5.5, customer.getId());
                } catch (CustomerNotFoundException e) {
                    e.printStackTrace();
                }
            });
            List<BankAccountDTO> bankAccountList = bankAccountService.bankAccountList();
            for (BankAccountDTO bankAccount : bankAccountList) {
                for (int i = 0; i < 10; i++) {
                    String accountId;
                    if (bankAccount instanceof SavingBankAccountDTO) {
                        accountId = ((SavingBankAccountDTO) bankAccount).getId();
                    } else {
                        accountId = ((CurrentBankAccountDTO) bankAccount).getId();
                    }
                    bankAccountService.credit(accountId, 10000 + Math.random() * 120000, "Credit");
                    bankAccountService.debit(accountId, 1000 + Math.random() * 9000, "Débit");
                }
            }
        };
    }
}
