package com.project.bankingManage.exceptions;

public class BalanceNotSufficientException extends Exception{

    public BalanceNotSufficientException(String message) {
        super(message);
    }
}
