package com.project.bankingManage.controller;

import com.project.bankingManage.dto.CustomerDTO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public interface CustomerController {

    @GetMapping(value = "/customers")
    List<CustomerDTO> customers();

    @GetMapping(value = "/customers/search")
    List<CustomerDTO> searchCustomers(@RequestParam(name = "keyword", defaultValue = "") String keyword);

    @GetMapping(value = "/customers/{id}")
    CustomerDTO getCustomer(@PathVariable(name = "id")  Long id);

    @PostMapping(value = "/customers")
    CustomerDTO saveCustomer(@RequestBody CustomerDTO customerDTO);

    @PutMapping(value = "/customers/{customerId}")
    CustomerDTO updateCustomer(@PathVariable(name = "customerId") Long customerId, @RequestBody CustomerDTO customerDTO);

    @DeleteMapping(value = "customers/{customerId}")
    void deleteCustomer(@PathVariable(name = "customerId") Long customerId);
}
