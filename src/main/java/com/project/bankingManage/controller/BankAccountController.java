package com.project.bankingManage.controller;

import com.project.bankingManage.dto.*;
import com.project.bankingManage.exceptions.BalanceNotSufficientException;
import com.project.bankingManage.exceptions.BankAccountNotFoundException;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public interface BankAccountController {

    @GetMapping(value = "/accounts/{accountId}")
    BankAccountDTO getBankAccount(@PathVariable String accountId) throws BankAccountNotFoundException;

    @GetMapping(value = "/accounts")
    List<BankAccountDTO> listAccounts();

    @GetMapping(value = "/accounts/{accountId}/operations")
    List<AccountOperationDTO> getHistory(@PathVariable String accountId);

    @GetMapping(value = "/accounts/{accountId}/pageOperations")
    AccountHistoryDto getAccountHistory(@PathVariable String accountId,
                                        @RequestParam(name = "page",defaultValue = "0") int page,
                                        @RequestParam(name = "size",defaultValue = "5") int size) throws BankAccountNotFoundException;

    @PostMapping(value = "/accounts/debit")
    DebitDTO debit(@RequestBody DebitDTO debitDTO) throws BankAccountNotFoundException, BalanceNotSufficientException;

    @PostMapping(value = "/accounts/credit")
    CreditDTO credit(@RequestBody CreditDTO creditDTO) throws BankAccountNotFoundException, BalanceNotSufficientException;

    @PostMapping(value = "/accounts/transfer")
    void transfer(@RequestBody TransferRequestDTO transferRequestDTO) throws BankAccountNotFoundException, BalanceNotSufficientException;


}
