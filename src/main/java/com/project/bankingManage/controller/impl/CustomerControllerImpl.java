package com.project.bankingManage.controller.impl;


import com.project.bankingManage.controller.CustomerController;
import com.project.bankingManage.dto.CustomerDTO;
import com.project.bankingManage.service.CustomerService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

/**
 * Controller user.
 */
@Component
@AllArgsConstructor
@Slf4j
public class CustomerControllerImpl implements CustomerController {

    /**
     * bankAccountService (couche service)
     */
    private CustomerService customerService;

    @Override
    public List<CustomerDTO> customers() {
        return this.customerService.listCustumers();
    }

    @Override
    public List<CustomerDTO> searchCustomers(String keyword) {
        return this.customerService.searchCustomers("%" + keyword + "%");
    }

    @Override
    public CustomerDTO getCustomer(Long customerId) {
        return this.customerService.getCustomer(customerId);
    }

    @Override
    public CustomerDTO saveCustomer(CustomerDTO customerDTO) {
        return this.customerService.saveCustomer(customerDTO);
    }

    @Override
    public CustomerDTO updateCustomer(Long customerId, CustomerDTO customerDTO) {
        customerDTO.setId(customerId);
        return this.customerService.updateCustomer(customerDTO);
    }

    @Override
    public void deleteCustomer(Long customerId) {
        this.customerService.deleteCustomer(customerId);
    }


}
