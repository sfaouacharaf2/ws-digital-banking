package com.project.bankingManage.controller.impl;

import com.project.bankingManage.controller.BankAccountController;
import com.project.bankingManage.dto.*;
import com.project.bankingManage.exceptions.BalanceNotSufficientException;
import com.project.bankingManage.exceptions.BankAccountNotFoundException;
import com.project.bankingManage.service.BankAccountService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@Component
@AllArgsConstructor
@Slf4j
public class BankAccountControllerImpl implements BankAccountController {

    private BankAccountService bankAccountService;

    @Override
    public BankAccountDTO getBankAccount(String accountId) throws BankAccountNotFoundException {
        return this.bankAccountService.getBankAccount(accountId);
    }

    @Override
    public List<BankAccountDTO> listAccounts() {
        return this.bankAccountService.bankAccountList();
    }

    @Override
    public List<AccountOperationDTO> getHistory(String accountId) {
        return this.bankAccountService.accountHistory(accountId);
    }

    @Override
    public AccountHistoryDto getAccountHistory(String accountId, int page, int size) throws BankAccountNotFoundException {
        return this.bankAccountService.getAccountHistory(accountId, page, size);
    }

    @Override
    public DebitDTO debit(DebitDTO debitDTO) throws BankAccountNotFoundException, BalanceNotSufficientException {
        this.bankAccountService.debit(debitDTO.getAccountId(), debitDTO.getAmount(), debitDTO.getDescription());
        return debitDTO;
    }

    @Override
    public CreditDTO credit(CreditDTO creditDTO) throws BankAccountNotFoundException, BalanceNotSufficientException {
        this.bankAccountService.credit(creditDTO.getAccountId(), creditDTO.getAmount(), creditDTO.getDescription());
        return creditDTO;
    }

    @Override
    public void transfer(TransferRequestDTO transferRequestDTO) throws BankAccountNotFoundException, BalanceNotSufficientException {
        this.bankAccountService.transfer(transferRequestDTO.getAccountSource(),
                transferRequestDTO.getAccountDestination(),
                transferRequestDTO.getAmount());
    }


}
