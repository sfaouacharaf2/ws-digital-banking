package com.project.bankingManage.dto;

import lombok.Data;

@Data
public class CreditDTO {

    private String accountId;
    private Double amount;
    private String description;
}
