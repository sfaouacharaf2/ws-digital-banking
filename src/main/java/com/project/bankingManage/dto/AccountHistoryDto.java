package com.project.bankingManage.dto;

import lombok.Data;

import java.util.List;

@Data
public class AccountHistoryDto {

    private String accountId;
    private double balance;
    private int currentPage;
    private int TotalPages;
    private int pageSize;
    private List<AccountOperationDTO> accountOperationDTOS;
}
